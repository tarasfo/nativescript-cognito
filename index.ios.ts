declare const AWSCognitoCredentialsProvider,
                AWSServiceConfiguration,
                AWSServiceManager,
                AWSRegionUSEast1;

export class Cognito {
    credentialsProvider: any;
    configuration: any;
    constructor(identityPoolId: string){
        this.credentialsProvider = AWSCognitoCredentialsProvider.alloc().initWithRegionType(AWSRegionUSEast1, identityPoolId);
        this.configuration = AWSServiceConfiguration.alloc().initWithRegion(AWSRegionUSEast1, this.credentialsProvider)
        AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = this.configuration;
        // AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1 identityPoolId:@"YourIdentityPoolId"];
        // AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
        // AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;

    }

    getCognitoId(){
        // NSString *cognitoId = credentialsProvider.identityId;
        let cognitoId = this.credentialsProvider.identityId;
        console.log("***************************************")
        console.log("Cognito getCognitoId: ", cognitoId)
        console.log("***************************************")
        return cognitoId
    }
}